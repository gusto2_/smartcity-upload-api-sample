
# README #

# Scope

This is an example client used with the Content-Upload API
of the CIRB SmartCity API Manager

# Build and usage

Project can be built using apache maven

```
mvn clean package
```


Upload content

```
java -jar ./target/upload-api-sample-0.0.1-jar-with-dependencies.jar -command upload -clientId XXXXXX -clientSecret YYYYY -remotePath "https://api.irisnetlab.be:443/api/content-upload/0.0.1/content/organization/dataset/filename" -file path_to_local_file
```


Download content

```
java -jar ./target/upload-api-sample-0.0.1-jar-with-dependencies.jar -command download -clientId XXXXXX -clientSecret YYYYY -remotePath "https://api.irisnetlab.be:443/api/content-upload/0.0.1/content/organization/dataset/filename" -file path_to_local_file
```

Where clientId and clientSecret parameter can be retrieved by registering an application
as described on the help page of the SmartCity API Manager 

https://api.irisnetlab.be/store/site/themes/cirb/help/help_page_01.html

What concerns the remote path: /content/organization/dataset/filename

 - organization - must be username of the user who registered the application
 - dataset and filename - any name consisting of letters, numbers, dot "." and underscore "_"
 

The user must have a role of "apim_content_upload" granted by administrators of CIRB

