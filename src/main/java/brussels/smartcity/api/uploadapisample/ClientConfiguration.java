
package brussels.smartcity.api.uploadapisample;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/**
 * Configuration sample, this contains static sample data
 * however these data should be read from a configuration file
 * 
 * this application is to be used as a sample client for the content-upload API
 * 
 * this configuration is assuming client_credentials OAuth client profile
 * 
 * Released under Creative Commons license
 * https://creativecommons.org/licenses/by/4.0/legalcode
 * @author Gabriel
 */
public class ClientConfiguration {
    

    private static final Logger LOG = Logger.getLogger(ClientConfiguration.class.getName());
    
    /**
     *  OAuth client_id of the registered application
     *  REPLACE WITH YOUR APPLICATION'S CLIENT_ID
     */
    private String clientId = "";
    /**
     *  OAuth client_secret of the registered application
     *  REPLACE WITH YOUR APPLICATION'S CLIENT_SECRET
     */    
    private String clientSecret = "";
    
    private String oauthTokenService = "https://api.irisnetlab.be:443/api/token";
    private String oauthRevokeService = "https://api.irisnetlab.be:443/api/revoke";
    
    private HttpClient httpClient;
    
    /**
     * timestamp until the access token is valid
     */
    private long tokenValidity;
    /**
     * token minimal validity window, if token will expire in defined time, we will 
     * request a new one
     */
    private static long TOKEN_MINIMAL_VALIDITY = 2000L; // 2 seconds
    private String oauthAccessToken;
    
    /**
     * OAuth access scopes <br>
     * for this sample we will request the apim_content_upload scope by default 
     */
    private String scopes = "apim_content_upload";
    
    
    /**
     * return a valid access token,
     * the access token is temporary based on the client application
     * 
     * the access token can be as well permanent generated in the API store
     * 
     * @return 
     */
    public String getAccessToken() throws IOException {
    
        synchronized(this) {
            
            // if provided access token is used, just return it
            if(this.tokenValidity<0) {
                return oauthAccessToken;
            }
            
            // if we have an access token and is invalid (or about to expire)
            // we will revoke it to request a new one
            if(this.oauthAccessToken!=null &&  this.tokenValidity<System.currentTimeMillis()+TOKEN_MINIMAL_VALIDITY) {
                // revoke existing token
                this.revokeAccessToken();
            }
            // request a new token
            this.requestAccessToken();
        }
        return this.oauthAccessToken;
    }
    
    /**
     * set static (permanent) access token
     * @param accessToken 
     */
    public void setAccessToken(String accessToken) {
      this.oauthAccessToken = accessToken;
      this.tokenValidity = -1L;
    }
    
     private void requestAccessToken() throws IOException {
            HttpClient client = this.getHttpClient();
            HttpPost revokeReq = new HttpPost(this.oauthTokenService);
            revokeReq.addHeader("Content-Type", "application/x-www-form-urlencoded");
            List<NameValuePair> revokeReqParams = new LinkedList<>();
            revokeReqParams.add(new BasicNameValuePair("client_id", this.clientId));
            revokeReqParams.add(new BasicNameValuePair("client_secret", this.clientSecret));
            revokeReqParams.add(new BasicNameValuePair("grant_type", "client_credentials"));
            if(this.scopes!=null) {
                revokeReqParams.add(new BasicNameValuePair("scope", this.scopes));
            }
            revokeReq.setEntity(new UrlEncodedFormEntity(revokeReqParams));
            try {
                HttpResponse resp = client.execute(revokeReq);
                if(resp.getStatusLine().getStatusCode()>200) {
                    LOG.log(Level.SEVERE, "Error requesting the access token {0} {1} {2}", 
                            new Object[] {
                                resp.getStatusLine().getStatusCode(),
                                resp.getStatusLine().getReasonPhrase(),
                                EntityUtils.toString(resp.getEntity()) 
                            }
                            );
                    throw new RuntimeException("Unable to request an access token: "+resp.getStatusLine().getReasonPhrase());
                }
                String jsonTokenString = EntityUtils.toString(resp.getEntity());
                JSONObject jsonToken = new JSONObject(jsonTokenString);
                
                this.oauthAccessToken = jsonToken.getString("access_token");
                this.tokenValidity = System.currentTimeMillis() + jsonToken.getLong("expires_in")*1000;
                
                EntityUtils.consume(resp.getEntity());
                
            } finally {
                revokeReq.releaseConnection();
            }
     }
    
    /**
     * Revoking the client will 
     * @throws IOException 
     */
    private void revokeAccessToken() throws IOException {
            HttpClient client = this.getHttpClient();
            HttpPost revokeReq = new HttpPost(this.oauthRevokeService);
            revokeReq.addHeader("Content-Type", "application/x-www-form-urlencoded");
            List<NameValuePair> revokeReqParams = new LinkedList<>();
            revokeReqParams.add(new BasicNameValuePair("client_id", this.clientId));
            revokeReqParams.add(new BasicNameValuePair("client_secret", this.clientSecret));
            revokeReqParams.add(new BasicNameValuePair("grant_type", "client_credentials"));
            revokeReqParams.add(new BasicNameValuePair("access_token", this.oauthAccessToken));
            revokeReq.setEntity(new UrlEncodedFormEntity(revokeReqParams));
            try {
                HttpResponse resp = client.execute(revokeReq);
                if(resp.getStatusLine().getStatusCode()>200) {
                    LOG.log(Level.SEVERE, "Error revoking the access token {0} {1} {2}", 
                            new Object[] {
                                resp.getStatusLine().getStatusCode(),
                                resp.getStatusLine().getReasonPhrase(),
                                EntityUtils.toString(resp.getEntity()) 
                            }
                            );
                    throw new RuntimeException("Unable to revoke current access token: "+resp.getStatusLine().getReasonPhrase());
                }
                EntityUtils.consume(resp.getEntity());
                
            } finally {
                revokeReq.releaseConnection();
            }
    }
    
    
    
    public HttpClient getHttpClient() {
        
        if(this.httpClient==null) {
            this.httpClient = HttpClientBuilder.create().build();
        }

        return this.httpClient;
        
    }

    public String getScopes() {
        return scopes;
    }

    public void setScopes(String scopes) {
        this.scopes = scopes;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getOauthTokenService() {
        return oauthTokenService;
    }

    public void setOauthTokenService(String oauthTokenService) {
        this.oauthTokenService = oauthTokenService;
    }

    public String getOauthRevokeService() {
        return oauthRevokeService;
    }

    public void setOauthRevokeService(String oauthRevokeService) {
        this.oauthRevokeService = oauthRevokeService;
    }
    
    
    
}
