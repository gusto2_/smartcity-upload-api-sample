/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brussels.smartcity.api.uploadapisample;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.util.EntityUtils;

/**
 * Upload sample, this contains static sample data however these data should be
 * read from a configuration file
 *
 * this application is to be used as a sample client for the content-upload API
 *
 * this configuration is assuming client_credentials OAuth client profile
 *
 * Released under Creative Commons license
 * https://creativecommons.org/licenses/by/4.0/legalcode
 *
 * @author Gabriel
 */
public class UploadClientSample {

    private static final Logger LOGGER = Logger.getLogger(UploadClientSample.class.getName());

    private ClientConfiguration oauthConfig;

    /**
     * upload content
     *
     * parameters: <ul>
     *
     * <li> -clientId - oauth client id
     * <li> -clientSecret - client
     * <li> -accessToken - access token if no client information is provided
     * <li> -remotePath - remote path in form of /organization/dataset/file,
     * where organization must be the username
     * <li> -file - local file reference
     * </ul>
     *
     *
     * @param args
     */
    public static void main(String[] args) {
        try {
            CommandLine cli = parseCli(args);
            //parse arguments
            String clientId = cli.getOptionValue("clientId");
            String clientSecret = cli.getOptionValue("clientSecret");
            String accessToken = cli.getOptionValue("accessToken");
            String remotePath = cli.getOptionValue("remotePath");
            String fileName = cli.getOptionValue("file");
            String command = cli.getOptionValue("command");
            
            if(command==null || (!"upload".equals(command) && !"download".equals(command))) {
                System.err.println("command must be upload or download");
                printUsage();
                System.exit(2);  
            }

            if (accessToken == null && (clientId == null || clientSecret == null)) {
                System.err.println("accessToken or clientId and clientSecret must be defined");
                printUsage();
                System.exit(2);
            }

            if (remotePath == null || fileName == null) {
                System.err.println("remotePath and file must be defined");
                printUsage();
                System.exit(2);
            }
            File inFile = new File(fileName);
            if (!inFile.exists()) {
                System.err.println("remotePath and file must be defined");
                printUsage();
                System.exit(2);
            }

            ClientConfiguration oauthConfig = new ClientConfiguration();
            if (accessToken != null) {
                oauthConfig.setAccessToken(accessToken);
            }
            if (clientId != null) {
                oauthConfig.setClientId(clientId);
                oauthConfig.setClientSecret(clientSecret);
            }

            UploadClientSample client = new UploadClientSample(oauthConfig);
            if("upload".equals(command)) {
                client.uploadFile(inFile, remotePath);
            } else {
                client.downloadFile(inFile, remotePath);
            }

        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "main", ex);
            System.exit(2);
        }
    }

    public UploadClientSample(ClientConfiguration oauthConfig) {
        this.oauthConfig = oauthConfig;
    }

    /**
     *
     * @param filePath
     * @param remoteParh
     */
    public void uploadFile(File file, String remotePath) throws IOException {

        HttpPost postReq = new HttpPost(remotePath);
        try {
            HttpClient client = this.oauthConfig.getHttpClient();
            postReq.addHeader("Authorization", "Bearer " + this.oauthConfig.getAccessToken());
            postReq.addHeader("Content-Type", "application/x-octet-stream");
            postReq.setEntity(new FileEntity(file));
            HttpResponse postResp = client.execute(postReq);
            String body = EntityUtils.toString(postResp.getEntity());
            EntityUtils.consume(postResp.getEntity());
            if (postResp.getStatusLine().getStatusCode() > 202) {
                LOGGER.log(Level.SEVERE, "Unable to upload: response {0} {1} {2}", new Object[]{
                    postResp.getStatusLine().getStatusCode(),
                    postResp.getStatusLine().getReasonPhrase(),
                    body
                });
                throw new RuntimeException("Unable to upload a file");
            } else {
                LOGGER.log(Level.INFO, "Content uploaded with status: {0}", postResp.getStatusLine().getStatusCode());
            }
        } finally {
            postReq.releaseConnection();
        }
    }

    /**
     * 
     * download the remote resource to the file defined
     * 
     * @param file
     * @param remotePath
     * @throws java.io.IOException
     */
    public void downloadFile(File file, String remotePath) throws IOException {

        HttpGet getReq = new HttpGet(remotePath);
        try 
                 {
            HttpClient client = this.oauthConfig.getHttpClient();
            getReq.addHeader("Authorization", "Bearer " + this.oauthConfig.getAccessToken());
            getReq.addHeader("Content-Type", "application/x-octet-stream");
            HttpResponse getResp = client.execute(getReq);
            if (getResp.getStatusLine().getStatusCode() > 202) {
                String body = EntityUtils.toString(getResp.getEntity());
                LOGGER.log(Level.SEVERE, "Unable to download: response {0} {1} {2}", new Object[]{
                    getResp.getStatusLine().getStatusCode(),
                    getResp.getStatusLine().getReasonPhrase(),
                    body
                });
                EntityUtils.consume(getResp.getEntity());
                throw new RuntimeException("Unable to download a file");
            } else {
                InputStream in = getResp.getEntity().getContent();
                OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
                byte[] buffer = new byte[4096];
                for (int readBytes = in.read(buffer); readBytes > -1; readBytes = in.read(buffer)) {
                    out.write(buffer, 0, readBytes);
                }
                out.flush();
                out.close();
                in.close();
                LOGGER.log(Level.INFO, "Content downloaded with status: {0}", getResp.getStatusLine().getStatusCode());
            }
        } finally {
            getReq.releaseConnection();
        }
    }

    private static void printUsage() {
        System.err.println("parameters: -command upload|download -accessToken  -clientId -clientSecret -remotePath -file");
    }

    private static CommandLine parseCli(String args[]) throws ParseException {
        Options cliOptions = new Options();
        cliOptions.addOption("command", true, "upload or download");
        cliOptions.addOption("clientId", true, "oauth client id");
        cliOptions.addOption("clientSecret", true, "oauth client secret");
        cliOptions.addOption("accessToken", true, "oauth access token");
        cliOptions.addOption("remotePath", true, "remoteResource /organization/dataset/filename");
        cliOptions.addOption("file", true, "local file to upload");

        CommandLineParser cliParser = new DefaultParser();
        return cliParser.parse(cliOptions, args);
    }
}
