
import brussels.smartcity.api.uploadapisample.ClientConfiguration;
import brussels.smartcity.api.uploadapisample.UploadClientSample;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.commons.codec.binary.StringUtils;
import org.junit.Assert;
import org.junit.Test;


/**
 *
 * @author GVCBB08
 */
public class OAuthClientTest {
    
    private static final String CLIENT_ID = "";
    private static final String CLIENT_SECRET = "";
    private static final String ACCESS_TOKEN = "";
    private static final String REMOTE_PATH = "https://api.irisnetlab.be:443/api/content-upload/0.0.1/content/admin/test/test.txt";
    
    @Test
    public void testWithClientCredentials() {
        this.testUploadWithClientInfo();
        this.testDownloadWithClientInfo();
    
    }
    
    @Test
    public void testWithAccessToken() {
      this.testUploadWithToken();
      this.testDownloadWithToken();
    }
    
    
//  @org.junit.Test
//   public void testTokenGeneration() {
//    try {
//        
//        ClientConfiguration oauthClient = new ClientConfiguration();
//        String accessToken = oauthClient.getAccessToken();
//        System.out.println(accessToken);
//        
//    }catch(Exception ex) {
//        ex.printStackTrace(System.out);
//        Assert.fail(ex.toString());
//    }
//   }
   
   /*
   * test upload file
   */

   public void testUploadWithClientInfo() {
       try {
           
           if(CLIENT_ID==null || CLIENT_SECRET==null || "".equals(CLIENT_ID) || "".equals(CLIENT_SECRET)) {
               System.out.println("clientId or secretId are null, skipping the test");
               return;
           }
           
           File tempFile = this.createTempFile();
           
           String[] args = new String[] {
               "-command", "upload",
               "-clientId", CLIENT_ID,
               "-clientSecret", CLIENT_SECRET,
               "-remotePath", REMOTE_PATH,
               "-file", tempFile.getAbsolutePath()
           };
           UploadClientSample.main(args);
           
       } catch(Exception ex) {
            ex.printStackTrace(System.out);
            Assert.fail(ex.toString());       
       }
   }
  
   /*
   * test upload file
   */

   public void testUploadWithToken() {
       try {
           
           if(ACCESS_TOKEN==null | "".equals(ACCESS_TOKEN)) {
               System.out.println("access token is empty, skipping the test");
               return;
           }
           
           File tempFile = this.createTempFile();
           
  
           String[] args = new String[] {
               "-command", "upload",
               "-accessToken", ACCESS_TOKEN,
               "-remotePath", REMOTE_PATH,
               "-file", tempFile.getAbsolutePath()
           };
           UploadClientSample.main(args);
           
           
       } catch(Exception ex) {
            ex.printStackTrace(System.out);
            Assert.fail(ex.toString());       
       }
   }
   
   
   /*
   * test upload file
   */

   public void testDownloadWithClientInfo() {
       try {
           
           if(CLIENT_ID==null || CLIENT_SECRET==null || "".equals(CLIENT_ID)||"".equals(CLIENT_SECRET)) {
               System.out.println("clientId or secretId are null, skipping the test");
               return;
           }
           
           File tempFile = this.createTempFile();
           
           String[] args = new String[] {
               "-command", "download",
               "-clientId", CLIENT_ID,
               "-clientSecret", CLIENT_SECRET,
               "-remotePath", REMOTE_PATH,
               "-file", tempFile.getAbsolutePath()
           };
           UploadClientSample.main(args);
           tempFile.delete();
           
       } catch(Exception ex) {
            ex.printStackTrace(System.out);
            Assert.fail(ex.toString());       
       }
   }
  
   /*
   * test upload file
   */

   public void testDownloadWithToken() {
       try {
           
           if(ACCESS_TOKEN==null | "".equals(ACCESS_TOKEN)) {
               System.out.println("access token is empty, skipping the test");
               return;
           }
           
           File tempFile = this.createTempFile();
           
  
           String[] args = new String[] {
               "-command", "download",
               "-accessToken", ACCESS_TOKEN,
               "-remotePath", REMOTE_PATH,
               "-file", tempFile.getAbsolutePath()
           };
           UploadClientSample.main(args);
           tempFile.delete();
           
       } catch(Exception ex) {
            ex.printStackTrace(System.out);
            Assert.fail(ex.toString());       
       }
   }   
   
   
   private File createTempFile() throws IOException {
       File tempFile = File.createTempFile("apitest", ".txt");
       PrintWriter out = new PrintWriter(tempFile);
       out.print("api upload test");
       out.flush();
       out.close();
       tempFile.deleteOnExit();
       return tempFile;
   }
   
}


